const express = require('express')
const app = express()
const sequelize = require('sequelize')
const db = require('./models')
const User = db.User
const port = 3000
const users = [
  { user: 'aaa', age: 13 },
  { user: 'bbb', age: 25 }
]

app.get('/', (req, res) => {
  const text = { messge: '歡迎來到我的API TEST說你好 rrree 叮叮 迪西 拉拉 小波dddd' }
  res.json(text)
})
app.get('/api/users', (req, res) => {
  User.findAll({
    raw: true,
    nest: true
  }).then((users) => res.json(users))
})

app.listen(port, () => {
  console.log(`operare server on port : ${port} successfully`)
})
