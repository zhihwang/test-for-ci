'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let data = Array.from({ length: 5 }).map((_, i) => ({
      email: `user${i + 1}@example.com`,
      password: '12345678',
      name: `user${i + 1}`,
      age: i * 5,
      createdAt: new Date(),
      updatedAt: new Date()
    }))

    await queryInterface.bulkInsert('Users',data,{})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users',null,{})
  }
};
